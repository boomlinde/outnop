outnop
======

Tool to extract the character data from c64 letter-noperator programs. The
timing data is excluded and the characters can be output by loading them
into the accumulator and calling chrout at `$ffd2`.

Usage
-----

    outnop file1 [file2 ... fileN]

The output files will have a .nop extension
